
HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=10000
setopt appendhistory autocd notify
zstyle :compinstall filename "$HOME/.zshrc"
autoload -Uz compinit
compinit
setopt COMPLETE_IN_WORD
zstyle ':completion:*' menu select
# End of lines added by compinstall

if [[ "$USER" == "root" ]]; then
  export PS1="%F{white}%B[ %f%F{yellow}%n%f%F{white}@%f%F{red}%M%f%F{white}: %1~ ]
%# %b%f"
else
  export PS1="%F{white}%B[ %f%F{yellow}%n%f%F{white}@%f%F{green}%M%f%F{white}: %1~ ]
%# %b%f"
fi

# aliases
alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -i"
alias ll="ls -lh --color=auto"
alias l="ls -lh --color=auto"
alias la="ls -lhA --color=auto"
alias a="ls -lhA --color=auto"
alias lsl="ls -lh --color=auto"
alias lsa="ls -lhA"
alias ..="cd .. && ls -lhA --color=auto"
alias s="sudo"
alias e="exit"
alias update="yum update"
alias install="yum install"
alias svim="sudo vim"
alias sctl="sudo systemctl"
alias lynx="lynx www.google.com.ua"
alias die="sudo shutdown now"
alias reborn="sudo shutdown -r now"
alias j="journalctl"
alias d="docker"
alias k="kubectl"
alias p="ping 8.8.8.8"
alias dc="docker-compose"