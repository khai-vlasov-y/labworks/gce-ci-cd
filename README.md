# Infrastructure automatization ansible playbooks

## Brief description

Stores useful playbooks, which helps to manage and setup new servers.

## Notes

Do not forget to set all needed variables for needed roles: `roles/*/*/defaults/main.yml` or in `roles/*/*/vars/main.yml` 

## Credentials
Update `inventory` and `roles/access/create-users/{defaults,files}`

## Contents

1. *bootstrap* - install `python2.7` with raw ssh script (it is needed for Ansible work)
2. *packages* - install common software and packages
	* *packages/upgrade* - upgrade all installed packages
	* *packages/base* - install zsh, mc, sudo, sed and else (common system utilities)
	* *packages/docker* - setup latest stable Docker version with [official script](get.docker.com)
3. *customization* - tweaks for effective manual work
	* *customization/zsh* - .zshrc with aliases and colors for shell
	* *customization/vimrc* - [basic vim setup](https://github.com/amix/vimrc)
	* *customization/timezone* - sets timezone (default: to UTC)
	* *customization/journald* - configure journald for storing configs after reboot and other settings
	* *customization/atop* - configuring atop - journal resource consumption
	* *customization/swap* - create swapfile
	* *customization/hostname* - sets hostname to name of server from inventory - `{{ inventory_hostname }}`
4. *access* - user accounts/access management
	* *access/create-users* - create users, generated random passwords in `~/passwd`.
	* *access/sshd-config* - replace `/etc/ssh/sshd_config` denying root login, allowing pubkey and password auth

## Examples
```bash
ansible-playbook all.yml --vault-password-file=./vault.pass -i inventory -l host1 -t access/sshd_config -t packages/base
ansible-playbook all.yml --vault-password-file=./vault.pass -i inventory # execute all playbooks for all hosts
ansible-playbook all.yml --vault-password-file=./vault.pass -i inventory -t customization/timezone
```

## Secrets
There is no secrets but anyway this notes are useful.  
All sercrets are placed in ansible-vault files - including hosts connect credetials.
It is easier to review and change secrets content with:
```
echo 'PasswordForVaults' > vault.pass
ansible-vault --vault-password-file=./vault.pass decrypt host_vars/*
```
Then you can change its content and encrypt it back.
```
ansible-vault --vault-password-file=./vault.pass encrypt host_vars/*
```